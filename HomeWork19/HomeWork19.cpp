﻿#include <iostream>
using namespace std;

class  Animal
{
public:
    virtual void Sound()
    {
        cout << "Hi" << endl;
    };
};

class Cow : public Animal
{
public:
    void Sound() override
    {
        cout << "Moo" << endl;
    }
};

class Chicken : public Animal
{
public:
    void Sound() override
    {
        cout << "Cococo" << endl;
    }
};

class Dog : public Animal
{
public:
    void Sound() override
    {
        cout << "Woof" << endl;
    }
};

int main()
{
    const int Size = 3;

    Animal* animals[Size] = { new Cow, new Chicken, new Dog };


    for (Animal* a : animals)
        a->Sound();
}
